import { useState } from "react";
import { useEffect } from "react";
import axios from "axios";
import { useLocation } from "react-router-dom";
import { useImmer } from "use-immer";

import Header from "./components/Header";
import ModalWrapper from "./components/Modal/ModalWrapper";
import ProductsContainer from "./components/ProductsContainer";
import AppRouter from "./AppRouter";

import "./App.css";
import "./styles.scss";

function App() {
  const URL = "./products.json";
  const { pathname } = useLocation();

  const [productId, setProductId] = useState("");
  const [products, setProducts] = useState([]);

  const [chosenProduct, setChosenProduct] = useState({});
  const [chosenCartProduct, setChosenCartProduct] = useState({});

  const [totalCount, setTotalCount] = useState(
    Number(localStorage.getItem("totalCount"))
  );

  const [isOpenFirstModal, setIsOpenFirstModal] = useState(false);
  const [isOpenSecondModal, setIsOpenSecondModal] = useState(false);

  const [cart, setCart] = useImmer([]);

  const addToCart = (productId) => {
    setCart((draft) => {
      const cartItem = draft.find(({ id }) => id === productId);

      if (!cartItem) {
        draft.push({ ...chosenProduct, count: 1 });
      } else {
        cartItem.count++;
      }

      const totalCount = draft.reduce((acc, el) => acc + el.count, 0);
      setTotalCount(totalCount);
      localStorage.setItem("totalCount", totalCount);

      localStorage.setItem("cart", JSON.stringify(draft));
    });
  };

  const decrementCartItem = (productId) => {
    setCart((draft) => {
      const cartItem = draft.find(({ id }) => id === productId);
      if (cartItem.count > 1) {
        cartItem.count--;
        localStorage.setItem("cart", JSON.stringify(draft));

        const totalCount = draft.reduce((acc, el) => acc + el.count, 0);
        setTotalCount(totalCount);
        localStorage.setItem("totalCount", totalCount);
      }
    });
  };

  const incrementCartItem = (productId) => {
    setCart((draft) => {
      const cartItem = draft.find(({ id }) => id === productId);
      cartItem.count++;
      localStorage.setItem("cart", JSON.stringify(draft));

      const totalCount = draft.reduce((acc, el) => acc + el.count, 0);
      setTotalCount(totalCount);
      localStorage.setItem("totalCount", totalCount);
    });
  };

  const deleteCartItem = () => {
    const updatedCart = cart.filter((el) => el.id !== chosenCartProduct.id);
    setCart(updatedCart);
    setChosenCartProduct({});
    localStorage.setItem("cart", JSON.stringify(updatedCart));

    const totalCount = updatedCart.reduce((acc, el) => acc + el.count, 0);
    setTotalCount(totalCount);
    localStorage.setItem("totalCount", totalCount);
  };

  useEffect(() => {
    const cartLS = localStorage.getItem("cart");

    if (cartLS) {
      setCart(JSON.parse(cartLS));
    }
  }, []);

  const [favoriteProducts, setFavoriteProducts] = useState(() => {
    const savedFavoriteProducts = localStorage.getItem("favoriteProducts");
    return savedFavoriteProducts ? JSON.parse(savedFavoriteProducts) : [];
  });

  useEffect(() => {
    const getProducts = async () => {
      try {
        const { data } = await axios.get(URL);
        setProducts(data);
      } catch (err) {
        console.log(err);
      }
    };

    getProducts();
  }, []);

  useEffect(() => {
    localStorage.setItem("favoriteProducts", JSON.stringify(favoriteProducts));
  }, [favoriteProducts]);

  const handleAddToFavourite = (id) => {
    const productToAdd = products.find((product) => product.id === id);

    const isAlreadyFavourite = favoriteProducts.some(
      (product) => product.id === id
    );

    const updatedFavorites = isAlreadyFavourite
      ? favoriteProducts.filter((favProduct) => favProduct.id !== id)
      : [...favoriteProducts, productToAdd];

    setFavoriteProducts(updatedFavorites);

    const updatedProducts = products.map((product) =>
      product.id === id
        ? { ...product, isFavourite: !isAlreadyFavourite }
        : product
    );

    setProducts(updatedProducts);
  };

  const openFirstModalHandler = () => {
    setIsOpenFirstModal(!isOpenFirstModal);
  };

  const openSecondModalHandler = (id) => {
    setIsOpenSecondModal(true);

    setProductId(id);

    setProducts((products) => {
      const newProducts = structuredClone(products);
      const product = newProducts.find((el) => id === el.id);
      setChosenProduct(product);
      setProducts(newProducts);
    });
  };

  const handleCloseModal = () => {
    setIsOpenSecondModal(false);
    setProductId("");
  };

  const countFavouriteProducts = () => {
    return favoriteProducts.length;
  };

  return (
    <>
      <Header
        countFavouriteProducts={countFavouriteProducts}
        totalCount={totalCount}
      />

      {pathname !== "/cart" && pathname !== "/favourites" && (
        <ProductsContainer
          products={products}
          handleAddToFavourite={handleAddToFavourite}
          openSecondModalHandler={openSecondModalHandler}
          favoriteProducts={favoriteProducts}
        />
      )}

      {isOpenFirstModal && (
        <ModalWrapper
          text={`Delete ${chosenCartProduct.name} ?`}
          bodyText={`To delete ${chosenCartProduct.name} from the cart, press "Yes".`}
          className="modal"
          hasImage="true"
          imagePath={chosenCartProduct.imagePath}
          footerCancelBtn="true"
          firstText="No"
          footerDeleteBtn="true"
          secondText="Yes"
          footerAddToFavBtn="false"
          thirdText=""
          modalIsOpen={openFirstModalHandler}
          handleCloseModal={openFirstModalHandler}
          deleteCartItem={deleteCartItem}
        />
      )}

      {isOpenSecondModal && (
        <ModalWrapper
          text={`Add to cart ${chosenProduct.name} ?`}
          className="modal"
          hasImage="true"
          imagePath={chosenProduct.imagePath}
          footerCancelBtn="false"
          footerDeleteBtn="false"
          footerAddToCart={true}
          thirdText="Yes, add to cart"
          modalIsOpen={openSecondModalHandler}
          handleCloseModal={handleCloseModal}
          productId={productId}
          addToCart={addToCart}
        />
      )}

      <AppRouter
        handleAddToFavourite={handleAddToFavourite}
        openSecondModalHandler={openSecondModalHandler}
        favoriteProducts={favoriteProducts}
        produducts={products}
        addedToCartProducts={cart}
        cart={cart}
        decrementCartItem={decrementCartItem}
        incrementCartItem={incrementCartItem}
        deleteCartItem={deleteCartItem}
        setIsOpenFirstModal={setIsOpenFirstModal}
        setChosenCartProduct={setChosenCartProduct}
      />
    </>
  );
}

export default App;
