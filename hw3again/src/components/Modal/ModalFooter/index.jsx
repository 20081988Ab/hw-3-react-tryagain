import Button from "../../Button";
import PropTypes from "prop-types";

const ModalFooter = ({
  footerCancelBtn,
  firstText,
  footerDeleteBtn,
  secondText,
  footerAddToCart = false,
  thirdText,
  modalIsOpen,
  handleCloseModal,
  productId,
  addToCart = () => {},
  deleteCartItem = () => {},

}) => {
  return (
    <div className="footer">
      {footerCancelBtn === "true" ? (
        <Button onClick={modalIsOpen} className="blueBtn">
          {" "}
          {firstText}
        </Button>
      ) : (
        ""
      )}
      {footerDeleteBtn === "true" ? (
        <Button
          onClick={() => {
            deleteCartItem();
            handleCloseModal();
          }}
          className="transparentBtn"
        >
          {" "}
          {secondText}
        </Button>
      ) : (
        ""
      )}
      {footerAddToCart === true ? (
        <Button
          onClick={() => {
            handleCloseModal();
            addToCart(productId);
          }}
          className="blueBtn"
        >
          {" "}
          {thirdText}
        </Button>
      ) : (
        ""
      )}
    </div>
  );
};

ModalFooter.propTypes = {
  footerCancelBtn: PropTypes.oneOf(["true", "false"]).isRequired,
  firstText: PropTypes.string,
  footerDeleteBtn: PropTypes.oneOf(["true", "false"]).isRequired,
  secondText: PropTypes.string,
  footerAddToCart: PropTypes.bool,
  thirdText: PropTypes.string.isRequired,
  modalIsOpen: PropTypes.func.isRequired,
  handleCloseModal: PropTypes.func.isRequired,
  countAddedToCartProducts: PropTypes.func.isRequired,
  productId: PropTypes.number.isRequired,
  addToCart: PropTypes.func,
  deleteCartItem: PropTypes.func,

};

export default ModalFooter;
