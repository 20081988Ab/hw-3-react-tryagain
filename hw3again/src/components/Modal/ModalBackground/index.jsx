import PropTypes from "prop-types";

const ModalBackground = ({ handleCloseModal }) => {
  return <div onClick={handleCloseModal} className="modalBackground"></div>;
};

ModalBackground.propTypes = {
  handleCloseModal: PropTypes.func.isRequired,
};

export default ModalBackground;
