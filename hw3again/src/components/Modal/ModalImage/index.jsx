 import styles from './ModalImage.module.scss';
 import PropTypes from 'prop-types'

 const ModalImage = ({imagePath}) => {

    return (
        <img className={styles.image} src={imagePath} alt="" ></img>
    )
 }

 ModalImage.propTypes = {
    imagePath: PropTypes.string,
  };

 export default ModalImage;