import styles from "./ProductCard.module.scss";
import PropTypes from "prop-types";

import StarSvg from "../svg/StarSvg";
import Button from "../Button";

const ProductCard = ({
  id,
  name,
  price,
  imagePath,
  handleAddToFavourite,
  isFavourite = false,
  openSecondModalHandler,


}) => {


  return (
    <div className={styles.card}>
      <div
        className={styles.image}
        style={{ backgroundImage: `url(${imagePath})` }}
      ></div>

      <div className={styles.productInfo}>
        <h2 className={styles.productName}>{name}</h2>
        <p className={styles.productPrice}> {price} ₴</p>
      </div>

      <div className={styles.svgWrapper}>
        <StarSvg
          placedOnCard={true}
          isFavourite={isFavourite}
          id={id}
          handleAddToFavourite={handleAddToFavourite}
        />
      </div>
      <Button
        type="button"
        onClick={() => { openSecondModalHandler(id) }}
        className={styles.addToCartBtn}
      >
        Add to cart
      </Button>
    </div>
  );
};

ProductCard.propTypes = {
  id: PropTypes.number.isRequired,
  name: PropTypes.string.isRequired,
  price: PropTypes.number.isRequired,
  imagePath: PropTypes.string.isRequired,
  handleAddToFavourite: PropTypes.func.isRequired,
  isFavourite: PropTypes.bool,
  openSecondModalHandler: PropTypes.func.isRequired,

};

export default ProductCard;
