import styles from "./NoItemsInCart.module.scss";

const NoItemsInCart = () => {
  return <h1 className={styles.noItemsNotification}>There is no added products.</h1>;
};

export default NoItemsInCart;
