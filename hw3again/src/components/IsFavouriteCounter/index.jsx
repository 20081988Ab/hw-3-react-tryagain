import StarSvg from "../svg/StarSvg";
import styles from "./IsFavouriteCounter.module.scss";
import PropTypes from "prop-types";
import { NavLink } from "react-router-dom";
import classnames from "classnames";

const IsFavouriteCounter = ({ countFavouriteProducts }) => {
  return (
    <NavLink
      to="/favourites"
      className={({ isActive }) =>
        classnames(styles.IsFavouriteCounterContainer, {
          [styles.active]: isActive,
        })
      }
    >
      <StarSvg
        headerStar="true"
        countFavouriteProducts={countFavouriteProducts}
      />

      <p className={styles.counter}>{countFavouriteProducts()}</p>
    </NavLink>
  );
};

IsFavouriteCounter.propTypes = {
  countFavouriteProducts: PropTypes.func.isRequired,
};

export default IsFavouriteCounter;
