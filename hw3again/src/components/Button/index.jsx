import PropTypes from "prop-types";

const Button = ({
  type = "button",
  className = "purpleBtn",
  onClick,
  children,
  count,

}) => {
  return (
    <button disabled={count <= 1} type={type} className={className} onClick={onClick}>
      {children}
    </button>
  );
};

Button.propTypes = {
  onClick: PropTypes.func,
  type: PropTypes.string,
  className: PropTypes.string,
  children: PropTypes.node.isRequired,
  count: PropTypes.number,
};

export default Button;
