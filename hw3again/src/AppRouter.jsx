import { Routes, Route } from "react-router-dom";
import CartContainer from "./routes/CartContainer";
import Favourites from "./routes/Favourites";
import ProductsContainer from "./components/ProductsContainer";
import PropTypes from "prop-types";

const AppRouter = ({
  handleAddToFavourite,
  openSecondModalHandler,
  favoriteProducts,

  products = [],
  cart = [],
  decrementCartItem = () => {},
  incrementCartItem = () => {},
  deleteCartItem = () => {},
  setIsOpenFirstModal = () => {},
  setChosenCartProduct = () => {},
}) => {
  return (
    <Routes>
      <Route
        path="/"
        element={
          <ProductsContainer
            products={products}
            handleAddToFavourite={handleAddToFavourite}
            openSecondModalHandler={openSecondModalHandler}
            favoriteProducts={favoriteProducts}
          />
        }
      />

      <Route
        path="/cart"
        element={<CartContainer 
          cart={cart} 
          decrementCartItem={decrementCartItem}
          incrementCartItem={incrementCartItem}
          deleteCartItem={deleteCartItem}
          setIsOpenFirstModal={setIsOpenFirstModal}
          setChosenCartProduct={setChosenCartProduct}
           />}
      />

      <Route
        path="/favourites"
        element={
          <Favourites
            products={favoriteProducts}
            handleAddToFavourite={handleAddToFavourite}
            openSecondModalHandler={openSecondModalHandler}
            favoriteProducts={favoriteProducts}
          />
        }
      />
    </Routes>
  );
};

AppRouter.propTypes = {
  handleAddToFavourite: PropTypes.func.isRequired,
  openSecondModalHandler: PropTypes.func.isRequired,
  favoriteProducts: PropTypes.array.isRequired,
  products: PropTypes.array,
  decrementCartItem: PropTypes.func,
  incrementCartItem: PropTypes.func,
  deleteCartItem: PropTypes.func,
  setIsOpenFirstModal: PropTypes.func,
  setChosenCartProduct: PropTypes.func,
  cart: PropTypes.array,
};

export default AppRouter;
